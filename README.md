  ## step 1: extract tag and version from git 
  ./ci/buildCurrentVersionName.sh
    
      will create 3 files that can be read with with branch, 
      version name and current tag
      build.name   , ie : master.20161103.155043.a727bf9 
      tag.name   ,  ie : master-v0.1 
      
      these files can be read from the ci, ie, with jenkins:
       def buildname = readFile 'build.name'
       def tagname = readFile 'tag.name'
       def branchname = readFile 'branch.name'
  ## step 2:
   build the image  
   `$$$ARGS$$$ ./ci/buildImage.sh`  
   ie:   
   ```bash
   IMAGENAME="docker-fatubuntu1604" DOCKER_REPO="quay.io/fravi" PUSH_SHA=1 PUSH_LATEST=1 PUSH_TAG=1 ./ci/buildImage.sh
   ```

   
   ### repository:
   this image is built with jenkins and can be found here : 
   quay.io/fravi/docker-fatubuntu1604
